import torch
from torch.nn import functional as F
from torch.utils.data import DataLoader

import pytorch_lightning as pl

from collections import OrderedDict

class MLP(pl.LightningModule):
    def __init__(self, batch_size,
                    learning_rate,
                    weight_decay,
                    train_optim,
                    loss,
                    metrics,
                    init_layers,
                    forward,
                    train_dataset,
                    val_dataset=None,
                    test_dataset=None):
        super().__init__()
        self.bsz = batch_size
        self.lr = learning_rate
        self.wd = weight_decay
        self.train_optim = train_optim
        self.loss = loss
        self.metrics = metrics
        self.forward_custom = forward
        self.train_dataset = train_dataset
        self.val_dataset = val_dataset
        self.test_dataset = test_dataset
        init_layers(self)
        self.init_metrics()

    def set_test_dataloader(self, test_dataset):
        self.test_dataset = test_dataset

    def forward(self, x):
        return self.forward_custom(self, x)

    def init_metrics(self):
        self.train_loss_mean = 0
        self.val_loss_mean = 0
        self.test_loss_mean = 0
        self.train_metric_mean = {}
        self.val_metric_mean = {}
        self.test_metric_mean = {}
        self.history = {'loss':[]}
        self.val_history = {'loss':[]}
        self.test_history = {'loss':[]}
        for key, value in self.metrics.items():
            self.train_metric_mean[key] = 0
            self.val_metric_mean[key] = 0
            self.test_metric_mean[key] = 0
            self.history[key] = []
            self.val_history[key] = []
            self.test_history[key] = []

    def train_dataloader(self):
        return  DataLoader(self.train_dataset, batch_size=self.bsz)

    def configure_optimizers(self):
        return self.train_optim(self.parameters(), lr=self.lr, weight_decay=self.wd)

    def val_dataloader(self):
        return  DataLoader(self.val_dataset, batch_size=self.bsz)

    def tqdm_keynames(self, key, phase):
        metric_name = phase + '_' + key
        mean_metric_name = phase + '_' + key + '_mean'
        return metric_name, mean_metric_name

    def training_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self(x)
        loss = self.loss(y_hat, y)
        tqdm_dict = {'train_loss': loss}
        # metrics
        for key, value in self.metrics.items():
            metric = value(y, y_hat)
            metric_name, mean_metric_name = self.tqdm_keynames(key, 'train')
            tqdm_dict[metric_name] = metric
            tqdm_dict[mean_metric_name] = self.train_metric_mean[key]
        output = OrderedDict({
            'loss': loss,
            'progress_bar': tqdm_dict,
        })

        return output

    def training_epoch_end(self, outputs):
        self.train_loss_mean = 0
        self.train_metric_mean = {}
        for key, value in self.metrics.items():
            self.train_metric_mean[key] = 0

        for output in outputs:
            train_loss = output['loss']
            self.train_loss_mean += train_loss
            for key, value in self.metrics.items():
                metric_key, mean_metric_key = self.tqdm_keynames(key, 'train')
                train_metric = output['progress_bar'][metric_key]
                self.train_metric_mean[key] += train_metric

        tqdm_dict = {'train_loss': train_loss}
        self.train_loss_mean /= len(outputs)
        self.history['loss'].append(self.train_loss_mean)
        for key, value in self.metrics.items():
            metric_key, mean_metric_key = self.tqdm_keynames(key, 'train')
            train_metric = output['progress_bar'][metric_key]
            self.train_metric_mean[key] /= len(outputs)
            self.history[key].append(self.train_metric_mean[key])
            tqdm_dict[metric_key] = train_metric
            tqdm_dict[mean_metric_key] = self.train_metric_mean[key]

        result = {'progress_bar': tqdm_dict, 'loss': train_loss}
        return result

    def validation_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self(x)
        loss = self.loss(y_hat, y)
        tqdm_dict = {'val_loss': loss}
        # metrics
        for key, value in self.metrics.items():
            metric = value(y, y_hat)
            metric_name, mean_metric_name = self.tqdm_keynames(key, 'val')
            tqdm_dict[metric_name] = metric
            tqdm_dict[mean_metric_name] = self.val_metric_mean[key]
        output = OrderedDict({
            'loss': loss,
            'progress_bar': tqdm_dict,
        })

        return output

    def validation_epoch_end(self, outputs):
        self.val_loss_mean = 0
        self.val_metric_mean = {}
        for key, value in self.metrics.items():
            self.val_metric_mean[key] = 0

        for output in outputs:
            val_loss = output['progress_bar']['val_loss']
            self.val_loss_mean += val_loss
            for key, value in self.metrics.items():
                metric_key, mean_metric_key = self.tqdm_keynames(key, 'val')
                val_metric = output['progress_bar'][metric_key]
                self.val_metric_mean[key] += val_metric

        tqdm_dict = {'val_loss': val_loss}
        self.val_loss_mean /= len(outputs)
        self.val_history['loss'].append(self.val_loss_mean)
        for key, value in self.metrics.items():
            metric_key, mean_metric_key = self.tqdm_keynames(key, 'val')
            #val_metric = output['progress_bar'][metric_key]
            self.val_metric_mean[key] /= len(outputs)
            self.val_history[key].append(self.val_metric_mean[key])
            tqdm_dict[metric_key] = val_metric
            tqdm_dict[mean_metric_key] = self.val_metric_mean[key]

        result = {'progress_bar': tqdm_dict, 'loss': val_loss}
        return result

    def test_dataloader(self):
        return DataLoader(self.test_dataset)

    def test_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self(x)
        loss = self.loss(y_hat, y)

        tqdm_dict = {'test_loss': loss}
        # metrics
        for key, value in self.metrics.items():
            metric = value(y, y_hat)
            metric_name, mean_metric_name = self.tqdm_keynames(key, 'test')
            tqdm_dict[metric_name] = metric
            tqdm_dict[mean_metric_name] = self.test_metric_mean[key]
        output = OrderedDict({
            'loss': loss,
            'progress_bar': tqdm_dict,
        })

        return output

    def test_epoch_end(self, outputs):
        self.test_loss_mean = 0
        self.test_metric_mean = {}
        for key, value in self.metrics.items():
            self.test_metric_mean[key] = 0

        for output in outputs:
            test_loss = output['progress_bar']['test_loss']
            self.test_loss_mean += test_loss
            for key, value in self.metrics.items():
                metric_key, mean_metric_key = self.tqdm_keynames(key, 'test')
                test_metric = output['progress_bar'][metric_key]
                self.test_metric_mean[key] += test_metric

        tqdm_dict = {'test_loss': test_loss}
        self.test_loss_mean /= len(outputs)
        self.test_history['loss'].append(self.test_loss_mean)
        for key, value in self.metrics.items():
            metric_key, mean_metric_key = self.tqdm_keynames(key, 'test')
            self.test_metric_mean[key] /= len(outputs)
            self.test_history[key].append(self.test_metric_mean[key])
            tqdm_dict[metric_key] = test_metric
            tqdm_dict[mean_metric_key] = self.test_metric_mean[key]

        result = {'progress_bar': tqdm_dict, 'loss': test_loss}
        return result
