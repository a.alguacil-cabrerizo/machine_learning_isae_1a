#! /bin/sh

if [ "$1" == "DDPG" ]; then
    agent=DDPG
elif [ "$1" == "TD3" ]; then
    agent=TD3
else
    echo "Error: usage is ./launch_tests.sh <agent>"
    echo "<agent> can be either DDPG or TD3"
    exit 1
fi


for f in results/${agent}/* 
do
    python3 test ${agent} --f=$f
done
